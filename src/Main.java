import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.* ;

class Main{

        public static void main(String []args) throws Exception {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            BigInteger x1 = new BigInteger(br.readLine().trim());
            BigInteger x2 = new BigInteger(br.readLine().trim());
            System.out.printf((karatsubaMultiply(x1, x2)).toString());
            System.out.println();
//            int N = Math.max(log10(x1), log10(x2));
//            System.out.println(N);

//            System.out.printf((x1.multiply(x2)).toString());
        }


        public static BigInteger karatsubaMultiply(BigInteger x1, BigInteger x2) {
<<<<<<< HEAD
            int n = Math.max(log10(x2), log10(x1));
            if (n <= 2){
                return x1.multiply(x2);} else {

                n = (n / 2);
                BigInteger m = BigInteger.TEN.pow(n);
                System.out.println("m: " + m + " N: " + n);
                BigInteger a = x1.divide(m);
                System.out.println("a: " + a);
                BigInteger b = x1.subtract(a.multiply(m));
                System.out.println("b: " + b);
                BigInteger c = x2.divide(m);
                System.out.println("c: " + c);
                BigInteger d = x2.subtract(c.multiply(m));
                System.out.println("d: " + d);
                BigInteger ac = karatsubaMultiply(a, c);
                BigInteger bd = karatsubaMultiply(b, d);
                BigInteger aMb = a.add(b);
                BigInteger cMd = c.add(d);

                BigInteger abcd = karatsubaMultiply(aMb, cMd);
                System.out.println("abcd: " + abcd);
            BigInteger adbc = abcd.subtract(ac).subtract(bd);
            System.out.println("adbc: "+adbc);
            if (adbc.equals(BigInteger.valueOf(8))){
                System.out.println(adbc);
            }
                BigInteger result = (BigInteger.TEN.pow(5).multiply(ac)).add(BigInteger.TEN.pow((int) 2.5).multiply((abcd).subtract(ac).subtract(bd)).add(bd));
                return result;
            }
            }
=======
            int N = Math.max(log10(x2), log10(x1));
            if (N < 2)
                return x1.multiply(x2);
            N = (N / 2) + (N % 2);
            BigInteger m = BigInteger.TEN.pow(N);
//            System.out.println("m: "+m+ " N: "+N );
            BigInteger a = x1.divide(m);
            System.out.println("a: "+a);
            BigInteger b = x1.subtract(a.multiply(m));
            System.out.println("b: "+b);
            BigInteger c = x2.divide(m);
            System.out.println("c: "+c);
            BigInteger d = x2.subtract(c.multiply(BigInteger.valueOf(N)));
            System.out.println("d: "+d);
            BigInteger ac = karatsubaMultiply(a, c);
            BigInteger bd = karatsubaMultiply(b, d);
            BigInteger abcd = karatsubaMultiply(a.add(b), c.add(d));
            BigInteger result =   x1;

//                    ac.add((abcd.subtract(ac).subtract(bd)).multiply(m)).add(bd.multiply( BigInteger.TEN.pow(N)));

            return result;
        }
>>>>>>> c90330060a6039d83fc063016404bd3395e30eb5

    private static int log10(BigInteger huge) {
        int digits = 0;
        int bits = huge.bitLength();
        // Serious reductions.
        while (bits > 4) {
            // 4 > log[2](10) so we should not reduce it too far.
            int reduce = bits / 4;
            // Divide by 10^reduce
            huge = huge.divide(BigInteger.TEN.pow(reduce));
            // Removed that many decimal digits.
            digits += reduce;
            // Recalculate bitLength
            bits = huge.bitLength();
        }
        // Now 4 bits or less - add 1 if necessary.
        if ( huge.intValue() > 9 ) {
            digits += 1;
        }
        return digits+1;
    }

    }

